FROM centos:centos6.10

#custom repo
RUN curl https://www.getpagespeed.com/files/centos6-eol.repo --output /etc/yum.repos.d/CentOS-Base.repo

#gcc
RUN yum install -y wget gcc gcc-c++ git icu.x86_64 libicu-devel.x86_64 &&\
	GCCVERSION=8.2.0 &&\
	mkdir /tmp/build &&\
	cd /tmp/build &&\
	wget http://mirror.koddos.net/gcc/releases/gcc-${GCCVERSION}/gcc-${GCCVERSION}.tar.gz &&\
	tar -xf  gcc-${GCCVERSION}.tar.gz &&\
	cd gcc-${GCCVERSION} &&\
	./contrib/download_prerequisites &&\
	./configure --enable-languages=c,c++ --disable-multilib &&\
	make -j $(nproc) &&\
	make install-strip &&\
	cd /tmp && yes | rm -R ./* &&\
	yum remove -y gcc gcc-c++ &&\
	yum clean all &&\
	cd /var/cache && rm -rf ./*

ENV LD_LIBRARY_PATH /usr/local/lib64:$LD_LIBRARY_PATH

#cmake
RUN  mkdir /tmp/build &&\
	CMAKEVERSION=3.14.2 &&\
	cd /tmp/build &&\
	wget https://github.com/Kitware/CMake/releases/download/v${CMAKEVERSION}/cmake-${CMAKEVERSION}.tar.gz &&\
	tar -xf cmake-${CMAKEVERSION}.tar.gz &&\
	cd cmake-${CMAKEVERSION} &&\
	./bootstrap &&\
	make -j $(nproc) &&\
	make install &&\
	cd /tmp && yes | rm -R ./*

#boost
RUN mkdir /tmp/build &&\
	BOOSTMAJOR=1 &&\
	BOOSTMINOR=68 &&\
	BOOSTHOTFIX=0 &&\
	cd /tmp/build &&\
	wget https://dl.bintray.com/boostorg/release/${BOOSTMAJOR}.${BOOSTMINOR}.${BOOSTHOTFIX}/source/boost_${BOOSTMAJOR}_${BOOSTMINOR}_${BOOSTHOTFIX}.tar.gz &&\
	tar -xf boost_${BOOSTMAJOR}_${BOOSTMINOR}_${BOOSTHOTFIX}.tar.gz &&\
	cd boost_${BOOSTMAJOR}_${BOOSTMINOR}_${BOOSTHOTFIX} &&\
	./bootstrap.sh &&\
	./b2 --without-python link=static -j $(nproc) cxxflags=-fPIC cflags=-fPIC install &&\
	cd /tmp && yes | rm -R ./*

#log4cplus
RUN mkdir /tmp/build &&\
	cd /tmp/build &&\
	git clone -b 1.2.x --depth 1 https://github.com/log4cplus/log4cplus &&\
	cd log4cplus &&\
	cmake . &&\
	make -j $(nproc) &&\
	make install &&\
    cd /tmp && yes | rm -R ./*

#Additional Packages
RUN yum install -y make yum-utils zlib zlib-devel xz perl pcre-devel bzip2-devel libffi-devel &&\
	yum clean all &&\
	cd /var/cache && rm -rf ./*

WORKDIR /home
