Контейнер, основанный на centos6.10. Содержит:  
gcc 8.2.0  
boost_1.68.0  
icu - из репозитория centos6  
log4cplus 1.2.2  
cmake 3.14.2

Дополнительные пакеты: make, yum-utils, zlib, zlib-devel, xz, perl, pcre-devel, bzip2-devel, libffi-devel
